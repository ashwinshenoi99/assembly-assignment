extern scanf
extern printf

section .data
	req: db "Enter your name",10,0
	formatstr: db "%s",0
	output: db "Hello, %s",10,0
	input: db 0

section .text

	global main

	main:
	push ebp
	mov ebp, esp
	
	push req
	push formatstr
	call printf
	
	push input
	push formatstr
	call scanf
	
	push input
	push output
	call printf
	
	mov esp, ebp
	pop ebp
	ret
	
	

