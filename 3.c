#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int main(int argc, char *argv[])
{
	int Rot;
	if (argc<2)
	{	
		printf("Usage: %s <string>\n",argv[0]);
		return 0;
	}
	char *text= argv[1];
	if (getenv("Rot")==NULL)
	{
		Rot=13;
	}
	else
	{
		Rot=atoi(getenv("Rot"));
	}
	
	for (int i=0; i<strlen(text); i++)
	{
		if(text[i]>=97&&text[i]<=122)
		{	
			text[i]=((text[i]+Rot-97)%26)+97;
		}
		else if(text[i]>=65&&text[i]<=90)	
		{				
			text[i]=((text[i]+Rot-65)%26)+65;
		}
	}
	printf("Encrypted Message: %s \n", text);
	return 0;
}

	
	

